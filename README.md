# rofi-pulse-select

[![pipeline status](https://gitlab.com/DamienCassou/rofi-pulse-select/badges/main/pipeline.svg)](https://gitlab.com/DamienCassou/rofi-pulse-select/-/commits/main)

Rofi-based interface to select source/sink (aka input/output) with PulseAudio.

## Usage

```session
$ rofi-pulse-select sink
$ rofi-pulse-select source
```
